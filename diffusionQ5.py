# Outer code for setting up the diffusion problem, calculate and plot

import matplotlib.pyplot as plt
import numpy as np

# read in all the schemes, intial conditions and other helper code

from diffusionSchemesQ5 import FTCS_fixed_zeroGrad, Analytical_Solution

### The main code is inside a function to avoid global variables ###

def main():
    """Solve the diffusion equation"""
    # Parameters
    zmin = 0
    zmax = 1e3
    nz = 21
    # nt6500 steady state
    nt = 10
    dt = 600
    # Unstable at 1800 dt
    K = 1
    Tinit = 290
    Q = -1.5/86400
    
    # height points
    z = np.linspace(zmin,zmax,nz)
    
    # Inital Condition
    T = Tinit*np.ones(nz)
    
    # Derived parameters
    dz = (zmax - zmin)/(nz-1)
    d = K*dt/dz**2 # Non-dimensional diffusion coefficient
    print("non-dimenstional diffusion coefficient= ", d)
    print(" dz = ", dz, " dt = ", dt, " nt = ", nt,)
    print("end time = ", nt*dt)
    
    # Diffusion using FTCS and BTCS
    T_FTCS = FTCS_fixed_zeroGrad(T.copy(), K, Q, dz, dt, nt)
    Analytic = Analytical_Solution(T.copy(), K, Q, dz, dt, nt)
    
    # Plot the solutions
    font = {'size'  : 20}
    plt.rc("font", **font)
    plt.plot(T_FTCS - Tinit, z, label="FTCS", color="blue", marker="+")
    plt.plot(Analytic - Tinit , z, label="Analytical", color="Red", marker="+")
    plt.ylim([0,1000])
    plt.legend(loc='best')
    plt.xlabel('$T-T-0$ (K)')
    plt.ylabel('z (m)')
    plt.tight_layout()
    plt.savefig('plots/FTCS_BTCS_unstable.jpg')
    plt.show()
    
main()