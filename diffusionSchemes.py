# Numerical schemes for simulating diffusion for outer code diffusion.py

import numpy as np

# The linear agebra package for BTCS

import numpy.linalg as la

def FTCS_fixed_zeroGrad(phi, K, Q, dx, dt ,nt):
    """Diffusion of profile in phi using FTCS using diffusion coefficient K,
    heating Q, grid spacing dx, time stop dt and for nt time steps.
    Boundary conditions are fixed at the start and zero gradient at the end"""
    
    nx = len(phi)
    
    # new time-step array for phi
    
    d = (K*dt)/(dx**2)
    phiNew = phi.copy()
    
    # FTCS for all time steps
    
    for i in range (nt):
        # loop over all internal points
        for it in range(1,nx - 1):
            phiNew[it] = phi[it] + d*(phi[it+1] - 2*phi[it] + phi[it-1]) + \
                dt*Q
        # Apply boundary conditions
        phiNew[0] = 293
        phiNew[-1] = phiNew[-2]       
        # Update phi for next time - step
        phi = phiNew.copy()
        
    return phi

def BTCS_fixed_zeroGrad(phi, K, Q, dx, dt ,nt):
    """Diffusion of profile in phi using BTCS using diffusion coefficient K,
    heating Q, grid spacing dx, time stop dt and for nt time steps.
    Boundary conditions are fixed at the start and zero gradient at the end"""
    
    nx = len(phi)
    
    # Non-dimensional diffusion coeffiicient
    d = dt*K/dx**2
    
    # arrqy representing BTCS
    M = np.zeros([nx,nx])
    # Fixed value boundary conditions at the start
    M[0,0] = 1
    # Zero gradient boundary conditions at the end
    M[-1,-1] = 1
    M[-1,-2] = -1
    # Other array elements
    for i in range(1,nx-1):
        M[i,i-1] = -d
        M[i,i] = 1+2*d
        M[i,i+1] = -d
        
    # BTCS for all time steps
    for i in range(nt):
        # RHS vector
        RHS = phi + dt*Q
        # RHS for fixed value boundary conditions at start
        RHS[0] = phi[0]
        # RHS for zero gradient boundary conditions at end
        RHS[-1] = 0
        
        # Solve the matrix equation to update phi
        
        phi = la.solve(M,RHS)
        
    return phi
        
    