# Numerical schemes for simulating diffusion for outer code diffusion.py

import numpy as np

# The linear agebra package for BTCS

import numpy.linalg as la

def FTCS_fixed_zeroGrad(phi, K, Q, dx, dt ,nt):
    """Diffusion of profile in phi using FTCS,
    grid spacing dx, time stop dt and for nt time steps.
    Boundary conditions are fixed at the start and zero gradient at the end"""
    
    nx = len(phi)
    
    # new time-step array for phi
    
    d = (K*dt)/(dx**2)
    phiNew = phi.copy()
    
    # FTCS for all time steps
    
    for i in range (nt):
        # loop over all internal points
        for it in range(1,nx - 1):
            phiNew[it] = (1-2*d)*phi[it] + d*(phi[it+1] + phi[it-1])
        # Apply boundary conditions
        phiNew[0] = 293
        phiNew[-1] = phiNew[-2]       
        # Update phi for next time - step
        phi = phiNew.copy()
        
    return phi

def Analytical_Solution(phi, K, Q, dx, dt ,nt):
    """Diffusion of profile in phi using analytical solution,
    grid spacing dx, time stop dt and for nt time steps."""
    nx = len(phi)
    phiNew = phi.copy()
    for i in range(nt):
        for it in range(1,nx - 1):
            phiNew[it] = np.exp(-K*dt*it*((np.pi*it)/phi[it])**2)*\
                np.sin((np.pi*it*dx*it)/phi[it])
        phiNew[0] = 293
        phiNew[-1] = phiNew[-2]    
        phi = phiNew.copy()
        
    return phiNew
        
    
